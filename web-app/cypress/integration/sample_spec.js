    // Test d'exemple par défaut :
    describe('The Traffic web site home page', () => {
        it('configuration loads', () => {
            cy.visit('/')

        })
    })

       describe('test 2 : number of segments', () => {
           it('have 28 segments', () => {
               //aller sur la page configuration
               cy.get('a[href="#configuration"]').click();

               //test sur le nombre de segments
               cy.get('form[data-kind="segment"]').should('have.length', 28);
           })
       })

       describe('test 3 : no vehicle', () => {
           it('have no vehicle', () => {
               //aller sur la page configuration
               cy.get('a[href="#configuration"]').click();

               //test sur l'interface qu'il n'y a pas de vehicule
               cy.contains("No vehicle available");

               //test sur le serveur de l’API en exploitant la route ‘elements‘.
               cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles');
               cy.get('@vehicles').should((response) => {

                   //test que le json est vide
                   expect(!Object.keys(response.body).length);
               })


           })
       })

       describe('test 4 : change speed segment', () => {
           it('speed segment 5 change to 30 ', () => {
               //modification du segment
               cy.get('a[href="#configuration"]').click();
               cy.get(":nth-child(5) > :nth-child(2) > .form-control").clear().type("30");
               cy.get('#segment-5 > .btn').click();

               //test sur la fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click()
               cy.get('.modal-header').should('not.exist');

               //test sur le serveur de l’API en exploitant la route ‘elements‘.
               cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
               cy.get('@elements').should((response) => {

                   //test de la vitesse du segment dans le json
                   expect(response.body['segments'][4]).to.have.property('speed', 30)
               })
           })
       })

       describe('test 5 : change roundabout capacity and duration', () => {
           it('capacity of roundabout to 4 and 15 duration', () => {
               //aller sur la page configuration
               cy.get('a[href="#configuration"]').click();

               //modification du rond point
               cy.get(':nth-child(3) > .card-body > form > :nth-child(2) > .form-control').clear().type("4");
               cy.get(':nth-child(3) > .card-body > form > :nth-child(3) > .form-control').clear().type("15");
               cy.get(':nth-child(3) > .card-body > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click()

               //test sur le serveur de l’API en exploitant la route ‘elements‘.
               cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
               cy.get('@elements').should((response) => {

                   //test de la capacité et de la durée du rond point dans le json
                   expect(response.body['crossroads'][2]).to.have.property('capacity', 4);
                   expect(response.body['crossroads'][2]).to.have.property('duration', 15);
               })

           })
       })

       describe('test 6 : change trafficlight orange green and next passage duration', () => {
           it('trafficlight 29 : change orange duration to 4, change green duration to 40 and passage duration to 8', () => {
               //aller sur la page configuration
               cy.get('a[href="#configuration"]').click();

               //modification du feu tricolore #29
               cy.get(':nth-child(1) > .card-body > form > :nth-child(2) > .form-control').clear().type("4");
               cy.get(':nth-child(1) > .card-body > form > :nth-child(3) > .form-control').clear().type("40");
               cy.get(':nth-child(1) > .card-body > form > :nth-child(4) > .form-control').clear().type("8");
               cy.get(':nth-child(1) > .card-body > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click()

               //test sur le serveur de l’API en exploitant la route ‘elements‘.
               cy.request('GET', 'http://127.0.0.1:4567/elements').as('elements')
               cy.get('@elements').should((response) => {

                   //test pour la durée de l'orange,vert et le passage au feu dans le json
                   expect(response.body['crossroads'][0]).to.have.property('orangeDuration', 4);
                   expect(response.body['crossroads'][0]).to.have.property('greenDuration', 40);
                   expect(response.body['crossroads'][0]).to.have.property('nextPassageDuration', 8);
               })
           })
       })

       describe('test 7 : Add vehicle in different segment', () => {
           it('add 3 vehicles', () => {
               //aller sur la page configuration
               cy.get('a[href="#configuration"]').click();

               //ajout du vehicule 1
               cy.get('form > :nth-child(1) > .form-control').clear().type("5");
               cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26");
               cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("50");
               cy.get('.col-md-4 > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click();

               //ajout du vehicule 2
               cy.get('form > :nth-child(1) > .form-control').clear().type("19");
               cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("8");
               cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("200");
               cy.get('.col-md-4 > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click();

               //ajout du vehicule 3
               cy.get('form > :nth-child(1) > .form-control').clear().type("27");
               cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("2");
               cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("150");
               cy.get('.col-md-4 > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click();

               //vérification de la présence des vehicules sur l'interface
               cy.get('.col-md-8 > .table > tbody > :nth-child(1) > :nth-child(2)').contains("200");
               cy.get('.col-md-8 > .table > tbody > :nth-child(2) > :nth-child(2)').contains("150");
               cy.get('.col-md-8 > .table > tbody > :nth-child(3) > :nth-child(2)').contains("50");

               //test sur le serveur de l’API en exploitant la route ‘vehicles‘.
               cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles')
               cy.get('@vehicles').should((response) => {

                   //test si les vehicules sont présents dans le json
                   expect(response.body['50.0'][0]).to.have.property('position', 0);
                   expect(response.body['200.0'][0]).to.have.property('position', 0);
                   expect(response.body['150.0'][0]).to.have.property('position', 0);
               })
           })
       })

       describe('test 8 : run simulation', () => {
           it('1 vehicle move', () => {
               //aller sur la page simulation
               cy.get('a[href="#simulation"]').click();

               //vérification que les vehicules sont à l'arrêt
               cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("block");

               //modification de la durée et lancement de la simulation
               cy.get('.form-control').clear().type("120");
               cy.get('.btn').click();

               //attendre que la simulation se termine
               cy.get('.progress-bar',{ timeout: 100000 }).should('have.attr','aria-valuenow','100');

               //test que la simulation est terminée
               cy.get('.progress-bar').should('have.attr','aria-valuenow','100');

               //test que seul le premier vehicule est en mouvement
               cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("play_circle_filled");

           })
       })

       describe('test 9 : no vehicle after simulation', () => {
           it('all vehicle move and out', () => {
               //rechargement de la page
               cy.reload();

               //aller sur la page simulation
               cy.get('a[href="#simulation"]').click();

               //test sur la page simulation qu'il n'y a pas de vehicule et que la simulation à bien été reintialisée
               cy.get('.col-md-7 > .table > tbody > tr > :nth-child(1)').contains("No vehicle available");
               cy.get('.progress-bar').should('have.attr','aria-valuenow','0');

               //aller sur la page configuration
               cy.get('a[href="#configuration"]').click();

               //ajout des mêmes vehicules que le scénario 7

               //ajout du vehicule 1
               cy.get('form > :nth-child(1) > .form-control').clear().type("5");
               cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26");
               cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("50");
               cy.get('.col-md-4 > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click();

               //ajout du vehicule 2
               cy.get('form > :nth-child(1) > .form-control').clear().type("19");
               cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("8");
               cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("200");
               cy.get('.col-md-4 > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click();

               //ajout du vehicule 3
               cy.get('form > :nth-child(1) > .form-control').clear().type("27");
               cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("2");
               cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("150");
               cy.get('.col-md-4 > form > .btn').click();

               //fenêtre de validation de modification
               cy.get('.modal-header').contains("Update information");
               cy.get('.modal-footer > .btn').click();

               //aller sur la page simulation
               cy.get('a[href="#simulation"]').click();

               //test que les vehicules sont à l'arrêt sur la page simulation
               cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("block");

               //modification de la durée et lancement de la simulation
               cy.get('.form-control').clear().type("500");
               cy.get('.btn').click();

               //attendre que la simulation se termine
               cy.get('.progress-bar',{ timeout: 150000 }).should('have.attr','aria-valuenow','100');

               //test que la simulation est terminée
               cy.get('.progress-bar').should('have.attr','aria-valuenow','100');

               //test que tous les vehicules sont à l'arrêt
               cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block");
               cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("block");

               //test sur le serveur de l’API en exploitant la route ‘vehicles‘.
               cy.request('GET', 'http://127.0.0.1:4567/vehicles').as('vehicles');
               cy.get('@vehicles').should((response) => {

                   //test que le json est vide
                   expect(!Object.keys(response.body).length);
               })

           })
       })

    describe('test 10 : simulation and check position of vehicles', () => {
        it('all vehicle move and test segment', () => {
            //rechargement de la page
            cy.reload();

            //aller sur la page simulation
            cy.get('a[href="#simulation"]').click();

            //test sur la page simulation qu'il n'y a pas de vehicule et que la simulation à bien été reintialisée
            cy.get('.col-md-7 > .table > tbody > tr > :nth-child(1)').contains("No vehicle available");
            cy.get('.progress-bar').should('have.attr','aria-valuenow','0');

            //aller sur la page configuration
            cy.get('a[href="#configuration"]').click();

            //ajout du vehicule 1
            cy.get('form > :nth-child(1) > .form-control').clear().type("5");
            cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26");
            cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("50");
            cy.get('.col-md-4 > form > .btn').click();

            //fenêtre de validation de modification
            cy.get('.modal-header').contains("Update information");
            cy.get('.modal-footer > .btn').click();

            //ajout du vehicule 2
            cy.get('form > :nth-child(1) > .form-control').clear().type("5");
            cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26");
            cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("80");
            cy.get('.col-md-4 > form > .btn').click();

            //fenêtre de validation de modification
            cy.get('.modal-header').contains("Update information");
            cy.get('.modal-footer > .btn').click();

            //ajout du vehicule 3
            cy.get('form > :nth-child(1) > .form-control').clear().type("5");
            cy.get('.col-md-4 > form > :nth-child(2) > .form-control').clear().type("26");
            cy.get('.col-md-4 > form > :nth-child(3) > .form-control').clear().type("80");
            cy.get('.col-md-4 > form > .btn').click();

            //fenêtre de validation de modification
            cy.get('.modal-header').contains("Update information");
            cy.get('.modal-footer > .btn').click();

            //aller sur la page simulation
            cy.get('a[href="#simulation"]').click();

            //test que les vehicules sont à l'arrêt sur la page simulation
            cy.get(':nth-child(1) > :nth-child(6) > div > .material-icons').contains("block");
            cy.get(':nth-child(2) > :nth-child(6) > div > .material-icons').contains("block");
            cy.get(':nth-child(3) > :nth-child(6) > div > .material-icons').contains("block");

            //modification de la durée et lancement de la simulation
            cy.get('.form-control').clear().type("200");
            cy.get('.btn').click();

            //attendre que la simulation se termine
            cy.get('.progress-bar',{ timeout: 100000 }).should('have.attr','aria-valuenow','100');

            //test que la simulation est terminée
            cy.get('.progress-bar').should('have.attr','aria-valuenow','100');

            //vérification des segments de chaque vehicules
            cy.get('tbody > :nth-child(1) > :nth-child(5)').contains("29");
            cy.get('tbody > :nth-child(2) > :nth-child(5)').contains("29");
            cy.get(':nth-child(3) > :nth-child(5)').contains("17");

        })
    })
    
